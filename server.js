'use strict';
/**
 * @author 柳正均2016-11-21
 * @class lucky.web_service.app
 * 導入模組，初始化模組參數，並設定所有路由事件。
 * {@img web-service_app.png 流程圖}
 */

process.setMaxListeners(20);

const morgan = require('morgan'); // http request紀錄器
const express = require('express'); // 用來進行host 網頁
const bodyParser = require('body-parser'); // 用來進行http body解析
const compression = require('compression');
const busboy = require('connect-busboy');
const port = 3000;

let serUtils = {
  'invokeRes': (res, data) => {
    if (!res.headersSent) {
      return res.send(data);
    }
    let stack = new Error().stack;
    console.error(`[ServerUtils][invokeRes] This res is aleady send. stack : ${stack}`);
  },
  'returnResult': (err, data = null) => {
    return {
      'error': err,
      'data': data
    };
  }
};
/**
 * 設定超時回應
 * @param  {Object}   req  請求物件實例
 * @param  {Object}   res  回應物件實例
 * @param  {Function} next 前往下一中介
 */
let requestTimeout = function (req, res, next) {
  res.setTimeout(20 * 1000, () => {
    console.error(`Response timeout! method:${req.method}, url:${req.originalUrl}, body:${JSON.stringify(req.body, 0, 2)}, ip:${req.ip}`);
    res.status(500);
    serUtils.invokeRes(res, '');
  });
  next();
};

let app = express();
app.set('case sensitive routing', true);// 路由區分大小寫
app.set('trust proxy', 1); // 取得發送端IP的設定
/*
██████  ███████  ██████      ████████ ██ ███    ███ ███████  ██████  ██    ██ ████████
██   ██ ██      ██    ██        ██    ██ ████  ████ ██      ██    ██ ██    ██    ██
██████  █████   ██    ██        ██    ██ ██ ████ ██ █████   ██    ██ ██    ██    ██
██   ██ ██      ██ ▄▄ ██        ██    ██ ██  ██  ██ ██      ██    ██ ██    ██    ██
██   ██ ███████  ██████         ██    ██ ██      ██ ███████  ██████   ██████     ██
                    ▀▀
*/
app.use(requestTimeout);// 設定超時回應
/*
██████  ███████  ██████      ██       ██████   ██████   ██████  ███████ ██████
██   ██ ██      ██    ██     ██      ██    ██ ██       ██       ██      ██   ██
██████  █████   ██    ██     ██      ██    ██ ██   ███ ██   ███ █████   ██████
██   ██ ██      ██ ▄▄ ██     ██      ██    ██ ██    ██ ██    ██ ██      ██   ██
██   ██ ███████  ██████      ███████  ██████   ██████   ██████  ███████ ██   ██
                    ▀▀
*/
app.use(morgan('common', {skip: (req, res) => res.statusCode === 204}));
/*
██████   ██████  ██████  ██    ██     ██████   █████  ██████  ███████ ███████ ██████
██   ██ ██    ██ ██   ██  ██  ██      ██   ██ ██   ██ ██   ██ ██      ██      ██   ██
██████  ██    ██ ██   ██   ████       ██████  ███████ ██████  ███████ █████   ██████
██   ██ ██    ██ ██   ██    ██        ██      ██   ██ ██   ██      ██ ██      ██   ██
██████   ██████  ██████     ██        ██      ██   ██ ██   ██ ███████ ███████ ██   ██
*/
app.use(bodyParser.json()); // 使用bodyParser將收到的json格式post解析成req.body物件。
app.use(bodyParser.urlencoded({
  extended: false
})); // 使用bodyParser將收到的urlencoded格式post解析成req.body物件。
/*
██████  ███████ ███████      ██████  ██████  ███    ███ ██████  ██████  ███████ ███████ ███████
██   ██ ██      ██          ██      ██    ██ ████  ████ ██   ██ ██   ██ ██      ██      ██
██████  █████   ███████     ██      ██    ██ ██ ████ ██ ██████  ██████  █████   ███████ ███████
██   ██ ██           ██     ██      ██    ██ ██  ██  ██ ██      ██   ██ ██           ██      ██
██   ██ ███████ ███████      ██████  ██████  ██      ██ ██      ██   ██ ███████ ███████ ███████
*/
app.use(compression()); // 設定compression用於將回傳資料壓縮。
/*
███████ ████████ ██████  ███████  █████  ███    ███     ██████   █████  ██████  ███████ ███████ ██████
██         ██    ██   ██ ██      ██   ██ ████  ████     ██   ██ ██   ██ ██   ██ ██      ██      ██   ██
███████    ██    ██████  █████   ███████ ██ ████ ██     ██████  ███████ ██████  ███████ █████   ██████
     ██    ██    ██   ██ ██      ██   ██ ██  ██  ██     ██      ██   ██ ██   ██      ██ ██      ██   ██
███████    ██    ██   ██ ███████ ██   ██ ██      ██     ██      ██   ██ ██   ██ ███████ ███████ ██   ██
*/
app.use(busboy({
  limits: {
    fileSize: 10 * 1024 * 1024 // 限制檔案大小2MB
  }
}));

/*
 ██████ ██████   ██████  ███████ ███████      ██████  ██████  ██  ██████  ██ ███    ██
██      ██   ██ ██    ██ ██      ██          ██    ██ ██   ██ ██ ██       ██ ████   ██
██      ██████  ██    ██ ███████ ███████     ██    ██ ██████  ██ ██   ███ ██ ██ ██  ██
██      ██   ██ ██    ██      ██      ██     ██    ██ ██   ██ ██ ██    ██ ██ ██  ██ ██
 ██████ ██   ██  ██████  ███████ ███████      ██████  ██   ██ ██  ██████  ██ ██   ████
*/
app.use((req, res, next) => {
  // 跨網域設定值
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Max-Age', '1800');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Content-Length, Accept');
  if (req.method === 'OPTIONS') {
    res.end();
  } else {
    next();
  }
});

app.post('/ntp', (req, res) => {
  let body = req.body;
  let result = [body.time, new Date().getTime()];
  console.log(`now:${new Date().getTime()}`);
  result.push(new Date().getTime());
  setTimeout(() => {
    console.log('sync');
  }, 1000);
  return serUtils.invokeRes(res, result);
});

app.post('/logtime', (req, res) => {
  let body = req.body;
  let time = new Date().getTime();
  console.log();
  console.log('serverTime:', time);
  console.log('clientTime:', body.time);
  console.log('diff:', time - body.time);
  return serUtils.invokeRes(res, '');
});

/*
      ██
     ██
    ██
   ██
  ██
  */

app.use('/', (req, res, next) => {
  if ((req.method === 'GET' || req.method === 'POST') && req.url === '/') { // 給AWS確認伺服器是否還活著
    res.status(204);
    return serUtils.invokeRes(res, '');
  }
  return next();
});

/*
  ██   ██  ██████  ██   ██
  ██   ██ ██  ████ ██   ██
  ███████ ██ ██ ██ ███████
       ██ ████  ██      ██
       ██  ██████       ██
  */
app.use((req, res, next) => {
  res.status(404);
  return serUtils.invokeRes(res, serUtils.returnResult(404));
});
app.listen(port);
console.log(`Listen:${port}`);
