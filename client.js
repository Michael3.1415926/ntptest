let request = require('request');
let host = 'http://13.115.42.31:3000';

setInterval(async () => {
  request.post({
    'timeout': 1000,
    'url': `${host}/ntp`,
    'json': {'time': new Date().getTime()}
  }, (err, data) => {
    if (err) {
      console.error(err);
    }
    let times = data.body;
    times.push(new Date().getTime());
    let delay = (times[3] - times[0]) - (times[2] - times[1]);
    let offset = ((times[1] - times[0]) + (times[2] - times[3])) / 2 || 0;
    let syncTime = new Date().getTime() + offset;
    setTimeout(() => {
      console.log('sync');
    }, 1000 + offset);
    request.post({
      'timeout': 1000,
      'url': `${host}/logtime`,
      'json': {'time': syncTime}
    }, () => {
      console.log(JSON.stringify({
        'delay': delay,
        'offset': offset,
        'syncTime': syncTime
      }, 0, 2));
    });
  });
}, 2000);
